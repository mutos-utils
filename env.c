/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <unistd.h>

#define VERSION "0.01"

extern char **environ;

int main(int argc, char* argv[])
{
    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp("--help", argv[arg]) == 0 || strcmp("-h", argv[arg]) == 0) {
            printf("Usage: %s [name=value ...] [command]\n", argv[0]);
            printf("\n"
                   "If no command is given, all environment variables are listed.\n"
                   "\n"
                   "    --help              Print this message.\n"
                   "    --version           Show version info.\n");
            return 0;
        } else if (strcmp("--version", argv[arg]) == 0 || strcmp("-v", argv[arg]) == 0) {
            printf("env (mutos) v"VERSION"\n");
            return 0;
        } else {
            // no more flags
            break;
        }
    }
    
    // parse the key-value pairs
    for ( ; arg < argc; arg++)
    {
        // find the equals
        char* equals = strchr(argv[arg], '=');
        // if there's no equals, it must be a command
        if (!equals) {
            break;
        }
        // delete it
        *equals = '\0';
        char* name  = argv[arg];
        // the value string starts right after the equals
        char* value = equals + 1;
        setenv(name, value, 1);
    }

    // parse command
    size_t total_len = 0;
    for (int i = arg; i < argc; i++)
    {
        total_len += strlen(argv[i]) + 1;
    }

    char* command = NULL;
    if (arg < argc) {
        command = calloc(total_len, sizeof(char));
    }

    for (int i = arg; i < argc; i++)
    {
        strcat(command, argv[i]);
        if (i != argc - 1) {
            strcat(command, " ");
        }
    }

    if (arg < argc) {
        command[total_len - 1] = '\0';
    }

    if (command) {
        system(command);
    } else {
        for (char **env = environ; *env; ++env)
        {
            printf("%s\n", *env);
        }
    }

    return 0;
}
