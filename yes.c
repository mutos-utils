/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, char *argv[])
{
    if (argc == 1) {
        for (;;)
        {
            printf("y\n");
        }
    }

    for (;;)
    {
        int i = 0;
        for (i = 1; i < (argc - 1); i++)
        {
            printf("%s ", argv[i]);
        }
        printf("%s\n", argv[argc-1]); // don't print a space for the last arg
    }

    return 0;
}
