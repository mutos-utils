/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>

#define VERSION "0.01"

int main(int argc, char* argv[])
{
    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp("--help", argv[arg]) == 0 || strcmp("-h", argv[arg]) == 0) {
            printf("Usage: %s [name] [suffix]\n", argv[0]);
            printf("\n"
                   "    --help              Print this message.\n"
                   "    --version           Show version info.\n");
            return 0;
        } else if (strcmp("--version", argv[arg]) == 0 || strcmp("-v", argv[arg]) == 0) {
            printf("basename (mutos) v"VERSION"\n");
            return 0;
        } else if (strcmp(argv[arg], "--") == 0) {
            arg++;
            break; 
        } else {
            break;
        }
    }

    if ((argc - arg) == 0) {
        fprintf(stderr, "%s: missing operand\n"
                        "Run '%s --help' for usage.\n",
                        argv[0], argv[0]);
        return 1;
    } else if ((argc - arg) > 2) {
        fprintf(stderr, "%s: too many arguments\n",
                         argv[0]);
        return 1;
    }

    char* name   = argv[arg];
    char* suffix = NULL;

    // check if string consists entirely of slash characters
    for (size_t i = 0; i < strlen(name); i++)
    {
        if (name[i] != '/') {
            break;
        }

        if (name[i] == '/' && i == strlen(name) - 1) {
            printf("/\n");
            return 0;
        }
    }

    // remove any trailing slashes
    for (size_t i = strlen(name) - 1; ; i--)
    {
        if (name[i] == '/') {
            name[i] = '\0';
        } else {
            // if there's no more slashes
            break;
        }

        // reached the beginning of the string
        if (i == 0) {
            break;
        }
    }

    // trim suffix (if there is one)
    if ((argc - arg) == 2) {
        suffix = argv[arg + 1];

        for (size_t i = strlen(name), j = strlen(suffix); ; i--, j--)
        {
            if (name[i - 1] == suffix[j - 1]) {
                name[i - 1] = '\0';
            } else {
                break;
            }
        }
    }

    // find the first slash
    size_t start = 0;
    for (size_t i = strlen(name) - 1; ; i--)
    {
        if (name[i] == '/') {
            start = i + 1;
            break;
        }

        // reached the beginning of the string
        if (i == 0) {
            start = i;
            break;
        }
    }

    // if suffix deleted the entire basename,
    // print the whole basename instead of
    // nothing
    if (strlen(name+start) == 0) {
        printf("%s", suffix);
    } else {
        printf("%s", name+start);
    }
    printf("\n");

    return 0;
}
