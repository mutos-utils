/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <getopt.h>

#define VERSION "0.01"

void usage(char* program)
{
    printf("Usage: %s [options] [file ...]\n", program);
    printf("Concatenates file(s) and/or standard input to standard output.\n"
           "\n"
           "    -u         (ignored)\n"
           "\n"
           "    --help     Print this message.\n"
           "    --version  Show version info.\n");
}

int main(int argc, char* argv[])
{
    // read from stdin if run with no args
    if (argc == 1) {
        int c = fgetc(stdin);
        while (c != EOF)
        {
            printf("%c", c);
            c = fgetc(stdin);
        }

        return 0;
    }


    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp(argv[arg], "-u") == 0) {
            ; // no-op
        } else if (strcmp(argv[arg], "--help") == 0){
            usage(argv[0]);
        } else if (strcmp(argv[arg], "--version") == 0) {
            printf("cat (mutos) v"VERSION"\n");
            return 0;
        } else if (strcmp(argv[arg], "--") == 0) { // the next args will be the actual args
            arg++;                                 // jump to first actual args
            break;                                 // and break out of flag loop
        } else {
            // done parsing flags
            break;
        }
    }

    for (; arg < argc; arg++)
    {
        FILE *current_file = NULL;

        if (strcmp(argv[arg], "-") == 0) {
            current_file = stdin;
        } else {
            current_file = fopen(argv[arg], "r");
        }


        if (current_file) {
            int c = fgetc(current_file);
            while (c != EOF)
            {
                printf("%c", c);
                c = fgetc(current_file);
            }
            fclose(current_file);
        } else {
            fprintf(stderr, "%s: %s: %s\n", argv[0], strerror(errno), argv[arg]);
        }
    }

    return 0;
}
