/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <getopt.h>

#define VERSION "0.01"

struct {
    bool append;
} flags;

void usage(char* program)
{
    printf("Usage: %s [options] [file ...]\n", program);
    printf("Copy standard input to each [file] and to standard output.\n"
           "\n"
           "    -a, --append  Run recursively.\n"
           "\n"
           "    --help        Print this message.\n"
           "    --version     Show version info.\n");
}

int main(int argc, char* argv[])
{
    bool errors = false;

    flags.append = false;
    static struct option long_options[] = {
        {"append",    no_argument, NULL, 'a'},
        {"help",      no_argument, NULL,  1},
        {"version",   no_argument, NULL,  2},
        {NULL, 0, NULL, 0}
    };

    int c = 0;
    while ((c = getopt_long(argc, argv, "a",
                            long_options, NULL)) != -1)
    {
        switch (c)
        {
            case 'a':
                flags.append = true;
                break;
            case 1:
                usage(argv[0]);
                return 0;
            case 2:
                printf("tee (mutos) v"VERSION"\n");
                return 0;
            default:
                fprintf(stderr,"Run '%s --help' for usage.\n",
                                     argv[0]);
                return 1;
        }
    }

    int file_count = argc - optind;
    FILE* outputs[file_count];
    memset(outputs, 0, sizeof(FILE*) * file_count);

    for (int i = optind; i < argc; i++)
    {
        if (strcmp(argv[i], "-") == 0) {
            outputs[i - optind] = stdout;
        } else {
            outputs[i - optind] = fopen(argv[i], flags.append ? "a" : "w");
            if (!outputs[i - optind]) {
                fprintf(stderr, "%s: %s: %s\n",
                                 argv[0], strerror(errno), argv[i]);
                errors = true;
            }
        }
    }
    
    int ch = fgetc(stdin);
    while (ch != EOF)
    {
        printf("%c", ch);
        for (int i = 0; i < file_count; i++)
        {
            if (outputs[i]) {
                fputc(ch, outputs[i]);
                fflush(outputs[i]);
            }
        }

        ch = fgetc(stdin);
    }

    if (errors) {
        return 1;
    } else {
        return 0;
    }
}
