/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>

#include <sys/stat.h>
#include <sys/types.h>

#define VERSION "0.01"

int main(int argc, char* argv[])
{
    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp("--help", argv[arg]) == 0 || strcmp("-h", argv[arg]) == 0) {
            printf("Usage: %s [src ...] [dest]\n", argv[0]);
            printf("\n"
                   "    --help              Print this message.\n"
                   "    --version           Show version info.\n");
            return 0;
        } else if (strcmp("--version", argv[arg]) == 0 || strcmp("-v", argv[arg]) == 0) {
            printf("cp (mutos) v"VERSION"\n");
            return 0;
        } else {
            break;
        }
    }

    if ((argc - arg) == 0) {
        fprintf(stderr, "%s: missing operand\n"
                        "Run '%s --help' for usage.\n",
                        argv[0], argv[0]);
        return 1;
    } else if ((argc - arg) == 1) {
        fprintf(stderr, "%s: missing destination\n"
                        "Run '%s --help' for usage.\n",
                        argv[0], argv[0]);
        return 1;
    }

    // exit code
    int ret = 0;

    struct stat src_stat_array[(argc - arg) - 1];
    FILE*       src_file_array[(argc - arg) - 1];

    int rc = 0;
                                  // - 1 because we are excluding the destination
    for (int i = 0; i < (argc - arg) - 1; i++)
    {
        rc = stat(argv[arg + i], &src_stat_array[i]);
        if (rc < 0) {
            fprintf(stderr, "%s: %s: %s\n",
                             argv[0], strerror(errno), argv[arg + i]);
            src_file_array[i] = NULL;
            ret = 1;
        } else {
            src_file_array[i] = fopen(argv[arg + i], "r");
        }
    }

    FILE* dest = NULL;
    struct stat dest_stat;

    rc = stat(argv[argc - 1], &dest_stat);
    if (rc < 0) {
        fprintf(stderr, "%s: %s: %s\n",
                         argv[0], strerror(errno), argv[arg + 1]);
        return 1;
    }

    if ((argc - arg) > 2 && !S_ISDIR(dest_stat.st_mode)) {
        fprintf(stderr, "%s: destination must be a directory: %s\n",
                         argv[0],                             argv[argc - 1]);
        return 1;
    }

    if (S_ISDIR(dest_stat.st_mode)) {
        for (int i = 0; i < (argc - arg) - 1; i++)
        {
            // Create the compound path if dest is a directory
            // (God, string concatenation in C is so obtuse.)
            char * dest_path = malloc(snprintf(NULL, 0, "%s%s%s",
                                                         argv[argc - 1],
                                                         // if the separator is missing, add it
                                                         (argv[argc - 1][strlen(argv[argc - 1]) - 1] == '/' ? "" : "/"),
                                                         argv[arg + i]) + 1);
            sprintf(dest_path, "%s%s%s", argv[argc - 1],
                                         // if the separator is missing, add it
                                         (argv[argc - 1][strlen(argv[argc - 1]) - 1] == '/' ? "" : "/"),
                                         argv[arg + i]);
            dest = fopen(dest_path, "w");
            if (!dest) {
                // if there was an error with the dest file,
                // print the error then skip over the file
                fprintf(stderr, "%s: %s: %s\n",
                                 argv[0], strerror(errno), dest_path);
                ret = 1;
                continue;
            }
            rc = stat(dest_path, &dest_stat);
            if (rc < 0) {
                fprintf(stderr, "%s: %s: %s\n",
                                 argv[0], strerror(errno), dest_path);
                ret = 1;
            } else {
                if (src_file_array[i]) {
                    int byte = fgetc(src_file_array[i]);
                    while (byte != EOF)
                    {
                        fputc(byte, dest);
                        byte = fgetc(src_file_array[i]);
                    }
                }
            }
            free(dest_path);
            fclose(dest);
        }
    }

    for (int i = 0; i < (argc - arg) - 1; i++)
    {
        if (src_file_array[i]) {
            fclose(src_file_array[i]);
        }
    }
    return ret;
}
