/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <pwd.h>
#include <grp.h>
#include <unistd.h>

#define VERSION "0.01"

int main(int argc, char* argv[])
{
    if (argc == 1) {
        fprintf(stderr, "%s: missing operand\n"
                        "Run '%s --help' for usage.\n",
                        argv[0], argv[0]);
        return 1;
    }

    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp(argv[arg], "--help") == 0) {
            printf("Usage: %s [owner][:group] [file ...]\n", argv[0]);
            return 0;
        } else if (strcmp(argv[arg], "--version") == 0) {
            printf("chown (mutos) v"VERSION"\n");
            return 0;
        } else {
            break;
        }
    }

    int colon_index = -1;
    for (size_t i = 0; i < strlen(argv[arg]); i++)
    {
        if (argv[arg][i] == ':') {
            colon_index = i;
            break;
        }
    }

    struct passwd *user  = NULL;
    struct group  *group = NULL;

    char* username  = NULL;
    char* groupname = NULL;

    if (colon_index == -1) {
        user = getpwnam(argv[arg]);
        username  = argv[arg];
    } else {
        username  = strndup(argv[arg], colon_index);
        groupname = strndup(argv[arg] + colon_index + 1, strlen(argv[arg] - colon_index - 1));

        user = getpwnam(username);
        group = getgrnam(groupname);
    }

    if (!user) {
        fprintf(stderr, "%s: invalid user: '%s'\n",
                         argv[0],           username);
        return 1;
    }

    if (!group && groupname) {
        fprintf(stderr, "%s: invalid group: '%s'\n",
                         argv[0],           groupname);
        return 1;
    }

    arg++; // go to next arg

    if (argc - arg < 1) {
        fprintf(stderr, "%s: No file specified\n", argv[0]);
        return 1;
    }

    for ( ; arg < argc; arg++)
    {
        int rc = chown(argv[arg], user->pw_uid, group ? group->gr_gid : (gid_t) -1);
        if (rc == -1) {
            fprintf(stderr, "%s: %s: %s\n", argv[0], strerror(errno), argv[arg]);
            return 1;
        }
    }

    return 0;
}
