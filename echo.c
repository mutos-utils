/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define VERSION "0.01"

struct {
    bool newline;
} flags;

int main(int argc, char* argv[])
{
    flags.newline = false;

    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp("-n", argv[arg]) == 0) {
            flags.newline = true;
        } else if (strcmp("--help", argv[arg]) == 0 || strcmp("-h", argv[arg]) == 0) {
            printf("Usage: %s [options] [string ...]\n", argv[0]);
            printf("\n"
                   "    -n                  Don't print the trailing newline.\n"
                   "\n"
                   "    --help              Print this message.\n"
                   "    --version           Show version info.\n");
            return 0;
        } else if (strcmp("--version", argv[arg]) == 0 || strcmp("-v", argv[arg]) == 0) {
            printf("echo (mutos) v"VERSION"\n");
            return 0;
        } else if (strcmp("--", argv[arg]) == 0) {
            arg++;
            break;
        } else {
            // no more flags
            break;
        }
    }

    for ( ; arg < argc; arg++)
    {
        printf("%s%s", argv[arg], arg != argc - 1 ? " " : (flags.newline ? "" : "\n"));
    }
    
    return 0;
}
