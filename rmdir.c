/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <getopt.h>
#include <unistd.h>

#define VERSION "0.01"

void usage(char* program)
{
    printf("Usage: %s [options] [file ...]\n", program);
    printf("Deletes directories.\n"
           "\n"
           "    --help     Print this message.\n"
           "    --version  Show version info.\n");
}

int main(int argc, char* argv[])
{
    bool errors = false;

    static struct option long_options[] = {
        {"help",      no_argument, NULL,  1},
        {"version",   no_argument, NULL,  2},
        {NULL, 0, NULL, 0}
    };

    int c = 0;
    while ((c = getopt_long(argc, argv, "",
                            long_options, NULL)) != -1)
    {
        switch (c)
        {
            case 1:
                usage(argv[0]);
                return 0;
            case 2:
                printf("rmdir (mutos) v"VERSION"\n");
                return 0;
            default:
                fprintf(stderr,"Run '%s --help' for usage.\n",
                                     argv[0]);
                return 1;
        }
    }

    for (int i = optind; i < argc; i++)
    {
        int rc = rmdir(argv[i]);
        if (rc != 0) {
            fprintf(stderr, "%s: %s: %s\n",
                             argv[0], strerror(errno), argv[i]);
            errors = true;
        }
    }

    if (errors) {
        return 1;
    }

    return 0;    
}
