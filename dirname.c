/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>

#define VERSION "0.01"

int main(int argc, char* argv[])
{
    if (argc < 2) {
        printf("%s: missing operand\n"
               "Run '%s --help' for usage.\n",
               argv[0], argv[0]);
        return 1;
    }

    char sep = '\n';

    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp(argv[arg], "-z") == 0 || strcmp(argv[arg], "--zero") == 0) {
            sep = '\0';
        } else if (strcmp(argv[arg], "-h") == 0 || strcmp(argv[arg], "--help") == 0) {
            printf("Usage: %s [options ...] [name ...]\n", argv[0]);
            printf("Output each [name] with its last non-slash component and trailing slashes removed.\n"
                   "If [name] contains no /'s, then output is '.' (meaning the current directory)."
                   "\n"
                   "    -z, --zero              Separate output with NUL rather than newline.\n"
                   "\n"
                   "    -h, --help              Print this message.\n"
                   "    -v, --version           Show version info.\n");
            return 0;
        } else if (strcmp(argv[arg], "-v") == 0 || strcmp(argv[arg], "--version") == 0) {
            printf("dirname (mutos) v"VERSION"\n");
            return 0;
        } else {
            // no more flags
            break;
        }
    }

    for ( ; arg < argc; arg++)
    {
        int last_slash = -1;
        for (size_t i = 0; i < strlen(argv[arg]); i++)
        {
            if (argv[arg][i] == '/' && i != strlen(argv[arg]) - 1) {
                last_slash = i;
            }
        }
        if (last_slash == -1) {
            argv[arg] = ".";
        } else {
            argv[arg][last_slash] = '\0';
        }
        printf("%s%c", argv[arg], sep);
    }
}
