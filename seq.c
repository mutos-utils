/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>  // atoi()
#include <stdbool.h>

#define VERSION "0.01"

struct {
    bool w;
} flags;

char* sep = "\n";
char* term = NULL;

void seq(char* first, char* increment, char* last)
{
    unsigned int width = 0;

    if (flags.w) {
        width = strlen(last);
    }

    for (int i = atoi(first); i <= atoi(last); i+=atoi(increment))
    {
        printf("%0*d%s", width, i, sep);
    }

    if (term) {
        printf("%s", term);
    }
}

int main(int argc, char* argv[])
{
    flags.w = false;

    if (argc == 1) {
        fprintf(stderr, "%s: missing operand\n"
                        "Run '%s --help' for usage.\n",
                        argv[0], argv[0]);
        return 1;
    }


    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp(argv[arg], "-s") == 0) {
            sep = argv[arg+1];
            arg++; // don't parse separator as a flag
        } else if (strcmp(argv[arg], "-t") == 0) {
            term = argv[arg+1];
            arg++; // don't parse terminator as a flag
        } else if (strcmp(argv[arg], "-w") == 0 || strcmp(argv[arg], "--equal-width") == 0) {
            flags.w = true;
        } else if (strcmp(argv[arg], "-h") == 0 || strcmp(argv[arg], "--help") == 0) {
            printf("Usage: %s [first] [last]\n", argv[0]);
            printf("\n"
                   "If [first] is omitted, it is assumed to be 1.\n");
            return 0;
        } else if (strcmp(argv[arg], "-v") == 0 || strcmp(argv[arg], "--version") == 0) {
            printf("seq (mutos) v"VERSION"\n");
            return 0;
        } else {
            break;
        }
    }

    int args_left = argc - arg;

    switch (args_left)
    {
        case 0:
            fprintf(stderr, "%s: too few arguments\n",
                             argv[0]);
            return 1;
        case 1:
            seq("1", "1", argv[arg]);
            break;
        case 2:
            seq(argv[arg], "1", argv[arg+1]);
            break;
        case 3:
            seq(argv[arg], argv[arg+1], argv[arg+2]);
            break;
        default:
            fprintf(stderr, "%s: too many arguments\n",
                            argv[0]);
            return 1;
    }

    return 0;
}
