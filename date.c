/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>

#define VERSION "0.01"

struct {
    bool iso;
    bool utc;
} flags;

char string[128+1] = {'\0'};
char* format = "%a %b %d %X %Z %Y";

int main(int argc, char* argv[])
{
    flags.iso = false;
    flags.utc = false;

    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp("--iso", argv[arg]) == 0 || strcmp("-I", argv[arg]) == 0) {
            flags.iso = true;
        } else if (strcmp("--utc", argv[arg]) == 0 || strcmp("-u", argv[arg]) == 0) {
            flags.utc = true;
        } else if (strcmp("--help", argv[arg]) == 0 || strcmp("-h", argv[arg]) == 0) {
            printf("Usage: %s [options ...] [+format]\n", argv[0]);
            printf("\n"
                   "    -I, --iso               Print time in ISO 8601 format.\n"
                   "    -u, --utc               Print time in UTC instead of localtime.\n"
                   "\n"
                   "    -h, --help              Print this message.\n"
                   "    -v, --version           Show version info.\n");
            return 0;
        } else if (strcmp("--version", argv[arg]) == 0 || strcmp("-v", argv[arg]) == 0) {
            printf("date (mutos) v"VERSION"\n");
            return 0;
        } else {
            // no more flags left
            break;
        }
    }

    if ((argc - arg) > 1) {
        fprintf(stderr, "%s: too many arguments\n",
                         argv[0]);
        return 1;
    } else if ((argc - arg) == 1) {
        if (argv[arg][0] != '+') {
            fprintf(stderr, "%s: invalid format\n",
                             argv[0]);
            return 1;
        } else if (flags.iso){
            fprintf(stderr, "%s: multiple formats given\n",
                             argv[0]);
            return 1;
        } else {
            format = argv[arg] + 1;
        }
    }

    time_t curr_time = time(NULL);
    struct tm *now = NULL;
    if (flags.utc) {
        now = gmtime(&curr_time);
    } else {
        now = localtime(&curr_time);
    }

    if (flags.iso) {
        format = "%Y-%m-%dT%X%z";
    }

    strftime(string, 128, format, now);

    printf("%s\n", string);

    return 0;
}
