/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>

int main()
{
    int c = fgetc(stdin);
    while (c != EOF)
    {
        int is_meta = 0;

        if (c > 127 && c < 256) {
            printf("M-");
            is_meta = 1;
            c -= 128;
        }

        if ((c >= ' ' && c <= '~') || ((c == '\t' || c == '\n') && !is_meta)) {
            printf("%c", c);
        } else if (c >= 0 && c < 32) {
            printf("^%c", c+64);
        }

        if (c == 127) {
            printf("^?");
        }

        c = fgetc(stdin);
    }

    return 0;
}
