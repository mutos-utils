/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>

#define VERSION "0.01"

int main(int argc, char* argv[])
{
    FILE* first_file  = NULL;
    FILE* second_file = NULL;

    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp("--help", argv[arg]) == 0 || strcmp("-h", argv[arg]) == 0) {
            printf("Usage: %s [file1] [file2]\n", argv[0]);
            printf("\n"
                   "If a file is '-' or missing, standard input is read.\n");
            return 0;
        } else if (strcmp("--version", argv[arg]) == 0 || strcmp("-v", argv[arg]) == 0) {
            printf("cmp (mutos) v"VERSION"\n");
            return 0;
        } else {
            break;
        }

    }

    switch (argc - arg) // number of args left
    {
        case 0:
            fprintf(stderr, "%s: missing operand\n"
                            "Run '%s --help' for usage.\n",
                            argv[0], argv[0]);
            return 2;
        case 1:
            first_file  = strcmp("-", argv[arg]) == 0 ? stdin : fopen(argv[arg], "r");
            second_file = stdin;
            break;
        case 2:
            first_file  = strcmp("-", argv[arg]) == 0 ? stdin : fopen(argv[arg], "r");
            second_file = strcmp("-", argv[arg+1]) == 0 ? stdin : fopen(argv[arg+1], "r");
            break;
        default:
            fprintf(stderr, "%s: too many arguments\n",
                             argv[0]);
            return 2;
    }

    if (first_file == second_file) {
        return 0;
    }

    char* first_name  = first_file == stdin ? "-" : argv[arg];
    char* second_name = second_file == stdin ? "-" : argv[arg+1];

    int differ = 0;

    int byte_count = 0;
    int line_count = 1;

    int first_byte  = 0;
    int second_byte = 0;

    
    while (first_byte != EOF && second_byte != EOF)
    {
        first_byte  = fgetc(first_file);
        second_byte = fgetc(second_file);
        byte_count++;

        if (first_byte != second_byte) {
            differ = 1;
            break;
        }

        if (first_byte == '\n') {
            line_count++;
        }
    }

    if (differ) {
        printf("%s %s differ: byte %d, line %d\n",
                first_name, second_name,  byte_count, line_count);
        fclose(first_file);
        fclose(second_file);
        return 1;
    } else {
        fclose(first_file);
        fclose(second_file);
        return 0;
    }

}
