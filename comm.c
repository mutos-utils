/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>

#define VERSION "0.01"

char* readline(FILE* stream)
{
    if (feof(stream)) {
        return NULL;
    }
    size_t length = 0;
    long int curr_pos = ftell(stream);

    int c = fgetc(stream);
    while (c != '\n' && c != EOF)
    {
        length++;
        c = fgetc(stream);
    }

    fseek(stream, curr_pos, SEEK_SET);


    char* line = calloc(1, length + 1);

    for (size_t i = 0; i < length; i++)
    {
        line[i] = fgetc(stream);
    }
    line[length] = '\0';

    fgetc(stream); // move past the newline

    return line;
}

void usage(char* program)
{
    printf("Usage: %s [options] [file1] [file2]\n", program);
    printf("Compares 2 sorted files line by line.\n"
           "\n"
           "    --help     Print this message.\n"
           "    --version  Show version info.\n"
           "\n"
           "If only one file is given, standard input is used for the second file.\n");
}

int main(int argc, char* argv[])
{
    static struct option long_options[] = {
        {"help",      no_argument, NULL,  1},
        {"version",   no_argument, NULL,  2},
        {NULL, 0, NULL, 0}
    };

    int c = 0;
    while ((c = getopt_long(argc, argv, "",
                            long_options, NULL)) != -1)
    {
        switch (c)
        {
            case 1:
                usage(argv[0]);
                return 0;
            case 2:
                printf("comm (mutos) v"VERSION"\n");
                return 0;
            default:
                fprintf(stderr,"Run '%s --help' for usage.\n",
                                     argv[0]);
                return 1;
        }
    }

    FILE* file1 = NULL;
    FILE* file2 = NULL;

    if ((argc - optind) < 1) {
        fprintf(stderr, "%s: missing operands\n"
                        "Run '%s --help' for usage.\n",
                        argv[0], argv[0]);
        return 1;
    } else if ((argc - optind) == 1) {
        file1 = fopen(argv[optind], "r");
        file2 = stdin;
    } else if ((argc - optind) == 2) {
        file1 = fopen(argv[optind],   "r");
        file2 = fopen(argv[optind+1], "r");
    } else {
        fprintf(stderr, "%s: extra operand: %s\n",
                         argv[0],           argv[optind+2]);
        return 1;
    }
    
    char* line1 = readline(file1);
    char* line2 = readline(file2);

    while (!feof(file1) || !feof(file2))
    {
        if (line1 && line2) {
            if (strcmp(line1, line2) == 0) {
                printf("\t\t%s\n", (line1 ? line1 : line2));
            } else {
                printf("\t%s\n", line2);
                printf("%s\n", line1);
            }

        } else {
            if (line1) {
                printf("%s\n", line1);
            }

            if (line2) {
                printf("\t%s\n", line2);
            }
        }
        free(line1);
        free(line2);

        line1 = readline(file1);
        line2 = readline(file2);
    }

    return 0;
}
