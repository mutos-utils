/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define VERSION "0.01"

int main(int argc, char* argv[])
{
    if (argc == 1) {
        fprintf(stderr, "%s: missing operand\n"
                        "Run '%s --help' for usage.\n",
                        argv[0], argv[0]);
        return 1;
    }

    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp(argv[arg], "--help") == 0) {
            printf("Usage: %s [newroot]\n", argv[0]);
            return 0;
        } else if (strcmp(argv[arg], "--version") == 0) {
            printf("chroot (mutos) v"VERSION"\n");
            return 0;
        } else {
            break;
        }
    }

    chdir(argv[arg]);
    int rc = chroot(argv[arg]);
        if (rc == -1) {
            fprintf(stderr, "%s: cannot change root directory to %s: %s\n", argv[0], argv[arg], strerror(errno));
            return 125;
        }

    return 0;
}
