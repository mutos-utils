/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <grp.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define VERSION "0.01"

int main(int argc, char* argv[])
{
    if (argc == 1) {
        fprintf(stderr, "%s: missing operand\n"
                        "Run '%s --help' for usage.\n",
                        argv[0], argv[0]);
        return 1;
    }

    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp(argv[arg], "--help") == 0) {
            printf("Usage: %s [group] [file ...]\n", argv[0]);
            return 0;
        } else if (strcmp(argv[arg], "--version") == 0) {
            printf("chgrp (mutos) v"VERSION"\n");
            return 0;
        } else {
            break;
        }
    }

    struct group *group = getgrnam(argv[arg]);

    if (!group) {
        fprintf(stderr, "%s: invalid group: '%s'\n", argv[0], argv[arg]);
        return 1;
    }

    arg++; // go to next arg

    if (argc - arg < 1) {
        fprintf(stderr, "%s: No file specified\n", argv[0]);
        return 1;
    }

    for ( ; arg < argc; arg++)
    {
        int rc = chown(argv[arg], -1, group->gr_gid);
        if (rc == -1) {
            fprintf(stderr, "%s: %s: %s\n", argv[0], strerror(errno), argv[arg]);
            return 1;
        }
    }

    return 0;
}
