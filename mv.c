/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>
#include <sys/stat.h>

#define VERSION "0.01"

void usage(char* program)
{
    printf("Usage: %s [options] [source ...] [dest]\n", program);
    printf("Moves files.\n"
           "\n"
           "    --help     Print this message.\n"
           "    --version  Show version info.\n");
}

int main(int argc, char* argv[])
{
    static struct option long_options[] = {
        {"help",      no_argument, NULL,  1},
        {"version",   no_argument, NULL,  2},
        {NULL, 0, NULL, 0}
    };

    int c = 0;
    while ((c = getopt_long(argc, argv, "",
                            long_options, NULL)) != -1)
    {
        switch (c)
        {
            case 1:
                usage(argv[0]);
                return 0;
            case 2:
                printf("mv (mutos) v"VERSION"\n");
                return 0;
            default:
                fprintf(stderr,"Run '%s --help' for usage.\n",
                                     argv[0]);
                return 1;
        }
    }

    if ((argc - optind) == 0) {
        fprintf(stderr, "%s: Missing operands\n"
                        "Run '%s --help' for usage.\n",
                         argv[0], argv[0]);
        return 1;
    } else if ((argc - optind) == 1) {
        
        fprintf(stderr, "%s: Missing destination operand\n"
                        "Run '%s --help' for usage.\n",
                         argv[0], argv[0]);
        return 1;
    }
    
    bool errors = false;

    char* dest_path = argv[argc - 1];
    bool  dest_dir  = false;

    struct stat dest;
    int rc = stat(dest_path, &dest);
    if (rc == 0) {
        // if destination is a directory
        if (dest.st_mode & S_IFDIR) {
            dest_dir = true;
        } else {
            // if multiple source files are specified
            // and dest isn't a directory
            if ((argc - 1) > 2) {
                fprintf(stderr, "%s: Destination is not a directory: %s\n",
                                 argv[0],                            dest_path);
                return 1;
            }
        }
    }

    for (int i = 1; i < argc - 1; i++)
    {
        if (dest_dir) {
            // concat dest path and filename
            size_t dest_len = strlen(dest_path) + 1 + strlen(argv[i]) + 1;
            char* dest = calloc(1, dest_len);
            if (!dest) {
                fprintf(stderr, "%s: %s: %s",
                                 argv[0], strerror(errno), argv[i]);
                errors = true;
            }
            
            strncat(dest, dest_path, dest_len - strlen(dest) - 1);
            strncat(dest, "/", dest_len - strlen(dest) - 1);
            strncat(dest, argv[i], dest_len - strlen(dest) - 1);
            dest[dest_len - 1] = '\0';

            rc = rename(argv[i], dest);
            if (rc != 0) {
                fprintf(stderr, "%s: %s: %s\n",
                                 argv[0], strerror(errno), argv[i]);
                errors = true;
            }

            free(dest);
        } else {
            rc = rename(argv[i], dest_path);
            if (rc != 0) {
                fprintf(stderr, "%s: %s: %s\n",
                                 argv[0], strerror(errno), argv[i]);
                return 1;
            }
        }
    }

    if (errors) {
        return 1;
    }

    return 0;
}
