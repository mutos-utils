/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <getopt.h>
#include <unistd.h>

#define VERSION "0.01"

// option flags
struct {
    bool symbolic;
} flags;

void usage(char* program)
{
    printf("Usage: %s [options] [mode] [file ...]\n", program);
    printf("Changes file permissions.\n"
           "\n"
           "    -s, --symbolic    Create a symbolic link.\n"
           "\n"
           "    --help            Print this message.\n"
           "    --version         Show version info.\n");
}

int main(int argc, char* argv[])
{
    char* linkname   = NULL;
    char* sourcename = NULL;

    flags.symbolic = false;
    static struct option long_options[] = {
        {"symbolic",  no_argument, NULL, 's'},
        {"help",      no_argument, NULL,  1},
        {"version",   no_argument, NULL,  2},
        {NULL, 0, NULL, 0}
    };

    int c = 0;
    while ((c = getopt_long(argc, argv, "s",
                            long_options, NULL)) != -1)
    {
        switch (c)
        {
            case 's':
                flags.symbolic = true;
                break;
            case 1:
                usage(argv[0]);
                return 0;
            case 2:
                printf("ln (mutos) v"VERSION"\n");
                return 0;
            default:
                fprintf(stderr,"Run '%s --help' for usage.\n",
                                     argv[0]);
                return 1;
        }
    }


    if ((argc - optind) < 1) {
        fprintf(stderr, "%s: missing operands\n"
                        "Run '%s --help' for usage.\n",
                        argv[0], argv[0]);
        return 1;
    } else if ((argc - optind) == 1) {
        sourcename = argv[optind];
        linkname   = argv[optind];
        for (size_t i = strlen(argv[optind]); i != 0; i--)
        {
            if (argv[optind][i] == '/') {
                linkname = argv[optind] + i + 1;
                break;
            }
        }
    } else if ((argc - optind) == 2) {
        sourcename = argv[optind];
        linkname   = argv[optind+1];
    } else {
        fprintf(stderr, "%s: extra operand: %s\n",
                         argv[0],           argv[optind+2]);
        return 1;
    }

    int rc = 0;
    if (flags.symbolic) {
        rc = symlink(sourcename, linkname);
    } else {
        rc = link(sourcename, linkname);
    }

    if (rc != 0) {
        fprintf(stderr, "%s: %s: %s\n",
                        argv[0], strerror(errno), linkname);
        return 1;
    }
    
    return 0;
}
