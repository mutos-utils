/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dirent.h>
#include <getopt.h>
#include <sys/types.h>

#define VERSION "0.01"

struct {
    bool all;
} flags;

int string_cmp(const void *a, const void *b)
{
    return strcasecmp(*(char* const*) a, *(char* const*) b);
}

int list_dir(char* dirname, bool multiple)
{
    DIR *dp = NULL;
    struct dirent *ep = NULL;

    size_t number = 0;
       
    dp = opendir(dirname);

    if (dp) {
        while ((ep = readdir(dp)))
        {
            number++;
        }
        closedir(dp);
    }

    char* entries[number];

    dp = opendir(dirname);
    if (dp) {
        for (size_t i = 0; (ep = readdir(dp)) && i < number; i++)
        {
            if (ep->d_type == DT_DIR) {
                entries[i] = strcat(ep->d_name, "/");
            } else {
                entries[i] = ep->d_name;
            }
        }
        closedir(dp);
    } else {
        return errno;
    }

    qsort(entries, number, sizeof(char*), string_cmp);

    if (multiple) {
        printf("%s:\n", dirname);
    }
    for (size_t i = 0; i < number; i++)
    {
        if (entries[i][0] == '.' && flags.all) {
            printf("%s\n", entries[i]);
        }

        if (entries[i][0] != '.') {
            printf("%s\n", entries[i]);
        }
    }

    return 0;
}

void usage(char* program)
{
    printf("Usage: %s [options]\n", program);
    printf("Lists directory contents.\n"
           "\n"
           "    -a, --all    Show hidden files also.\n"
           "\n"
           "    --help       Print this message.\n"
           "    --version    Show version info.\n");
}

int main(int argc, char* argv[])
{
    bool errors = false;

    flags.all = false;
    static struct option long_options[] = {
        {"all",       no_argument, NULL, 'a'},
        {"help",      no_argument, NULL,  1},
        {"version",   no_argument, NULL,  2},
        {NULL, 0, NULL, 0}
    };

    int c = 0;
    while ((c = getopt_long(argc, argv, "a",
                            long_options, NULL)) != -1)
    {
        switch (c)
        {
            case 'a':
                flags.all = true;
                break;
            case 1:
                usage(argv[0]);
                return 0;
            case 2:
                printf("ls (mutos) v"VERSION"\n");
                return 0;
            default:
                fprintf(stderr,"Run '%s --help' for usage.\n",
                                     argv[0]);
                return 1;
        }
    }

    if ((argc - optind) == 0) {
        if (list_dir("./", false) != 0) {
            fprintf(stderr, "%s: %s: %s\n",
                         argv[0], strerror(errno), realpath("./", NULL));
            return 1;
        }
    } else if ((argc - optind) == 1) {
        if (list_dir(argv[optind], false) != 0) {
            fprintf(stderr, "%s: %s: %s\n",
                         argv[0], strerror(errno), realpath("./", NULL));
            return 1;
        }
    }

    for (int i = optind; i < argc; i++)
    {
        if (list_dir(argv[i], true) != 0) {
            fprintf(stderr, "%s: %s: %s\n",
                             argv[0], strerror(errno), argv[i]);
            errors = true;
        } else if (i != argc - 1) {
            printf("\n");
        }
    }

    if (errors) {
        return 1;
    } else {
        return 0;
    }
}
