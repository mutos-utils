/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>
#include <inttypes.h>

#include <sys/types.h>
#include <sys/stat.h>

#define VERSION "0.01"

int main(int argc, char* argv[])
{
    char* input_path  = NULL;
    char* output_path = NULL;
    uint64_t bs = 512;
    uint32_t bs_modifier = 1;
    unsigned int count = 0;
    unsigned int r_in  = 0;
    unsigned int r_out = 0;

    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strstr(argv[arg], "if=")) {
            input_path  = strstr(argv[arg], "if=") + strlen("if=");
        } else if (strstr(argv[arg], "of=")) {
            output_path = strstr(argv[arg], "of=") + strlen("of=");
        } else if (strstr(argv[arg], "bs=")) {
            char* bs_value = strstr(argv[arg], "bs=") + strlen("bs=");
            char  bs_unit  = bs_value[strlen(bs_value) - 1];
            // if last character isn't a digit
            if (!isdigit(bs_unit)) {
                switch(bs_unit)
                {
                    case 'k':
                    case 'K':
                        bs_modifier = 1024;
                        break;
                    case 'm':
                    case 'M':
                        bs_modifier = 1024 * 1024;
                        break;
                    case 'g':
                    case 'G':
                        bs_modifier = 1024 * 1024 * 1024;
                        break;
                    default:
                        fprintf(stderr, "%s: invalid unit: '%c'\n",
                                         argv[0],           bs_unit);
                        return 1;
                }
                printf("%c\n", bs_unit);
                // delete the unit, leaving just the number
                bs_value[strlen(bs_value) - 1] = '\0';
            }

            // validate input
            for (size_t i = 0; i < strlen(bs_value); i++)
            {
                if (!isdigit(bs_value[i])) {
                    fprintf(stderr, "%s: invalid number: '%s'\n",
                                     argv[0],             bs_value);
                    return 1;
                }
            }

            bs = atoi(bs_value);
            if (bs == 0) {
                    fprintf(stderr, "%s: invalid number: '%"PRIu64"'\n",
                                     argv[0],             bs);
                    return 1;
                }

        } else if (strstr(argv[arg], "count=")) {
            char* count_value = strstr(argv[arg], "count=") + strlen("count=");

            // validate input
            for (size_t i = 0; i < strlen(count_value); i++)
            {
                if (!isdigit(count_value[i])) {
                    fprintf(stderr, "%s: invalid number: '%s'\n",
                                     argv[0],             count_value);
                    return 1;
                }
            }

            count = atoi(count_value);
            if (count == 0) {
                fprintf(stderr, "0+0 records in\n"
                                "0+0 records out\n"
                                "0 bytes (0 B) copied\n");
                return 0;
            }
        } else if (strcmp("--help", argv[arg]) == 0 || strcmp("-h", argv[arg]) == 0) {
            printf("Usage: %s\n", argv[0]);
            printf("[delim] must be a character and [field] starts at 1.\n"
                   "\n"
                   "    -h, --help              Print this message.\n"
                   "    -v, --version           Show version info.\n");
            return 0;
        } else if (strcmp("--version", argv[arg]) == 0 || strcmp("-v", argv[arg]) == 0) {
            printf("dd (mutos) v"VERSION"\n");
            return 0;
        } else {
            // no more flags left
            break;
        }
    }

    FILE* input_file  = NULL;
    FILE* output_file = NULL;

    int rc = 0;

    if (!input_path || strcmp(input_path, "-") == 0) {
        input_file = stdin;
    } else {
        struct stat input_stat;
        rc = stat(input_path, &input_stat);
        if (rc < 0) {
            fprintf(stderr, "%s: %s: %s\n",
                             argv[0], strerror(errno), input_path);
            return 1;
        }
        input_file = fopen(input_path, "r");
    }

    if (!input_file) {
        fprintf(stderr, "%s: %s: %s\n",
                         argv[0], strerror(errno), input_path);
        return 1;
    }

    if (!output_path) {
        output_file = stdout;
    } else {
        struct stat output_stat;
        rc = stat(output_path, &output_stat);
        if (rc < 0 && errno != ENOENT) {
            fprintf(stderr, "%s: %s: %s\n",
                             argv[0], strerror(errno), input_path);
            return 1;
        }
        output_file = fopen(output_path, "w");
    }

    if (!output_file) {
        fprintf(stderr, "%s: %s: %s\n",
                         argv[0], strerror(errno), output_path);
        return 1;
    }

    int c = '\0';
    int bytes = 0;

    c = fgetc(input_file);
    while (c != EOF)
    {
        bytes++;
        if (bytes % (bs*bs_modifier) == 0) {
            r_in++;
        }
        fputc(c, output_file);
        if (bytes % (bs*bs_modifier) == 0) {
            r_out++;
        }
        c = fgetc(input_file);
    }

    fprintf(stderr, "%d+0 records in\n"
                    "%d+0 records out\n"
                    "%d bytes copied\n",
                     r_in, r_out, bytes);
    return 0;
}
