/*  
    Copyright © 2013 Alastair Stuart
    
    This program is open source software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/types.h>

#define VERSION "0.01"

char delim = '\0';
unsigned int field = 0;

int main(int argc, char* argv[])
{
    // flag parsing
    int arg = 0;
    for (arg = 1; arg < argc; arg++)
    {
        if (strcmp("-d", argv[arg]) == 0) {
            delim = argv[arg + 1][0];
            arg++;
        } else if (strcmp("-f", argv[arg]) == 0) {
            field = atoi(argv[arg + 1]);
            arg++;
        } else if (strcmp("--help", argv[arg]) == 0 || strcmp("-h", argv[arg]) == 0) {
            printf("Usage: %s -d [delim] -f [field] [file]\n", argv[0]);
            printf("[delim] must be a character and [field] starts at 1.\n"
                   "\n"
                   "    -h, --help              Print this message.\n"
                   "    -v, --version           Show version info.\n");
            return 0;
        } else if (strcmp("--version", argv[arg]) == 0 || strcmp("-v", argv[arg]) == 0) {
            printf("cut (mutos) v"VERSION"\n");
            return 0;
        } else {
            // no more flags left
            break;
        }
    }

    if (!field) {
        fprintf(stderr, "%s: you must specify a field.\n"
                        "Run '%s --help' for usage.\n",
                         argv[0], argv[0]);
        return 1;
    }

    FILE* file   = NULL;
    char* buffer = NULL;

    struct stat file_stat;
    int buf_size = 0;
    // if no file is specified or file is "-"
    if ((argc - arg) == 0 || strcmp(argv[arg], "-") == 0) {
        buffer = calloc(4096 + 1, sizeof(char));
        buf_size = 4096 + 1;
        file = stdin;
    } else {
        // stat the file to get its size
        // if we statted successfully,
        // allocate a buffer
        int rc = stat(argv[argc - 1], &file_stat);
        if (rc >= 0) {
            buffer = calloc(file_stat.st_size + 1, sizeof(char));
            buf_size = file_stat.st_size + 1;
            file = fopen(argv[arg], "r");
        } else {
            fprintf(stderr, "%s: %s: %s\n",
                             argv[0], strerror(errno), argv[argc - 1]);
            return 1;
        }
    }

    int num_lines = 0;

    int c = fgetc(file);
    for (int i = 0; i < buf_size && c != EOF; i++)
    {
         buffer[i] = (char)c;
         c = fgetc(file);
    }
    // Null-terminate the buffer
    buffer[buf_size - 1] = '\0';

    // for each char in the buffer
    for (int i = 0; i < buf_size; i++)
    {
        if (buffer[i] == '\n') {
            num_lines++;
        }
    }

    size_t line_len[num_lines];
    memset(line_len, 0, num_lines * sizeof(size_t));

    char* lines[num_lines];
    // for each line: i = current line, j = current character
    for (int i = 0, j = 0, last_enter = -1; i < num_lines && j < file_stat.st_size; j++)
    {
        if (buffer[j] != '\n') {
            line_len[i]++;
        } else {
            // allocate a string that's the length of the current line
            lines[i] = malloc(line_len[i] + 1);
            // copy the line
            strncpy(lines[i], buffer+last_enter+1, line_len[i]);
            // set last_enter to the current enter
            // for the next line
            last_enter = j;
            // go to next line
            i++;
        }
    }

    // delete delimiter characters by setting it to -1
    for (int i = 0; i < num_lines; i++)
    {
        for (size_t j = 0; j < strlen(lines[i]); j++)
        {
            if (lines[i][j] == delim) {
                lines[i][j] = -1;
            }
        }
    }

    // go through the line and print the requested field
    for (int i = 0; i < num_lines; i++)
    {
        for (size_t j = 0, field_count = 0; j < strlen(lines[i]); j++)
        {
            if (lines[i][j == 0 ? j : j - 1] == -1 && lines[i][j] != -1) {
                field_count++;
            }

            if (field_count == field - 1 && lines[i][j] != -1) {
                printf("%c", lines[i][j]);
            }
        }
        printf("\n");
    }

    return 0;
}
